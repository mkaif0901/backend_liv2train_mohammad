### What is this repository for? ###

This is an Springboot application for an startup Liv2Train. I have created an MVP for a registry for Govt funded Training Centers with some requirements. 
In the MVP, I have created a Spring project which saves 'training center data' into Database which can be retrieve in future for further processing.
* Version
0.0.1-SNAPSHOT

## Requirements

For building and running the application you need:

- [JDK 1.8](http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html)
- [Maven 3](https://maven.apache.org)
- [MongoDB](https://www.mongodb.com/try/download/community)

## Running the application locally

One way is to open the project in any IDE and execute the `main` method in the
`com/liv2train/mvp/MvpApplication.java` class.

Alternatively you can use the [Spring Boot Maven plugin](https://docs.spring.io/spring-boot/docs/current/reference/html/build-tool-plugins-maven-plugin.html) 
like so:

```shell
mvn spring-boot:run
```