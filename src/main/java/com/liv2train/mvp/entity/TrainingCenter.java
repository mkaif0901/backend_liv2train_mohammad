package com.liv2train.mvp.entity;

import com.liv2train.mvp.model.Address;
import lombok.AccessLevel;
import lombok.Data;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Data
@Document
public class TrainingCenter {

    @Id
    @Setter(AccessLevel.PROTECTED)
    private String id;

    private String centerName;

    private String centerCode;

    private Address address;

    private int studentCapacity;

    private List<String> coursesOffered;

    private long createdOn;

    private String contactEmail;

    private String contactPhone;

}
