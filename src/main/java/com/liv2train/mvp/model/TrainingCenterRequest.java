package com.liv2train.mvp.model;

import lombok.Data;

import javax.validation.constraints.*;
import java.util.List;

@Data
public class TrainingCenterRequest {

    @NotBlank(message = "Please provide centerName")
    @Size(max = 40, message = "centerName : maximum of 40 chars allowed")
    private String centerName;

    @NotBlank(message = "Please provide centerCode")
    @Pattern(regexp="(^$|[a-zA-Z0-9]{12})", message = "Only 12 alphanumeric chars allowed")
    private String centerCode;

    @NotNull(message = "Please provide address")
    private Address address;

    @PositiveOrZero(message = "Please provide valid student capacity")
    private int studentCapacity;

    private List<String> coursesOffered;

    @NotBlank
    @Email(message = "Please provide valid email id")
    private String contactEmail;

    @NotBlank(message = "Please provide phone number")
    @Pattern(regexp="(^$|[0-9]{10})", message = "Please provide valid phone number")
    private String contactPhone;
}
