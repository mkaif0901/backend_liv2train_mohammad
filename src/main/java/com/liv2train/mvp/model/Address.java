package com.liv2train.mvp.model;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class Address {

    private String detailedAddress;

    private String city;

    private String state;

    private String pinCode;
}
