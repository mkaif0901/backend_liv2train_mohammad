package com.liv2train.mvp.controller;

import com.liv2train.mvp.entity.TrainingCenter;
import com.liv2train.mvp.model.TrainingCenterRequest;
import com.liv2train.mvp.service.TrainingCenterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/training-center")
public class TrainingCenterController {

    @Autowired
    private TrainingCenterService trainingCenterService;

    @PostMapping("/create")
    public TrainingCenter createTrainingCenter(@RequestBody @Valid TrainingCenterRequest request) {
        return trainingCenterService.createTrainingCenter(request);
    }

    @GetMapping("/get/all")
    public List<TrainingCenter> getAllTrainingCenter() {
        return trainingCenterService.getTrainingCenter();
    }
}
