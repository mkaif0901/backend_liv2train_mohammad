package com.liv2train.mvp.service.impl;

import com.liv2train.mvp.repo.TrainingCenterRepository;
import com.liv2train.mvp.entity.TrainingCenter;
import com.liv2train.mvp.model.TrainingCenterRequest;
import com.liv2train.mvp.service.TrainingCenterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TrainingCenterServiceImpl implements TrainingCenterService {

    @Autowired
    private TrainingCenterRepository repository;

    @Override
    public TrainingCenter createTrainingCenter(TrainingCenterRequest request) {
        TrainingCenter trainingCenter = populateTrainingCenter(request);
        return repository.save(trainingCenter);
    }

    @Override
    public List<TrainingCenter> getTrainingCenter() {
        return repository.findAll();
    }

    private TrainingCenter populateTrainingCenter(TrainingCenterRequest request) {
        TrainingCenter trainingCenter = new TrainingCenter();
        trainingCenter.setCenterCode(request.getCenterCode());
        trainingCenter.setCenterName(request.getCenterName());
        trainingCenter.setContactEmail(request.getContactEmail());
        trainingCenter.setContactPhone(request.getContactPhone());
        trainingCenter.setCoursesOffered(request.getCoursesOffered());
        trainingCenter.setCreatedOn(System.currentTimeMillis());
        trainingCenter.setStudentCapacity(request.getStudentCapacity());
        trainingCenter.setAddress(request.getAddress());
        return trainingCenter;
    }
}
