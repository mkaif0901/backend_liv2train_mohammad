package com.liv2train.mvp.service;

import com.liv2train.mvp.entity.TrainingCenter;
import com.liv2train.mvp.model.TrainingCenterRequest;

import java.util.List;

public interface TrainingCenterService {

    TrainingCenter createTrainingCenter(TrainingCenterRequest request);

    List<TrainingCenter> getTrainingCenter();
}
